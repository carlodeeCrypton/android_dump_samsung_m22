## m22jvxx-user 13 TP1A.220624.014 M225FVXXU8DWH4 release-keys
- Manufacturer: samsung
- Platform: mt6768
- Codename: m22
- Brand: samsung
- Flavor: m22jvxx-user
- Release Version: 13
- Kernel Version: 4.14.186
- Id: TP1A.220624.014
- Incremental: M225FVXXU8DWH4
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Treble Device: true
- Locale: en-GB
- Screen Density: undefined
- Fingerprint: samsung/m22jvxx/m22:12/SP1A.210812.016/M225FVXXU8DWH4:user/release-keys
- OTA version: 
- Branch: m22jvxx-user-13-TP1A.220624.014-M225FVXXU8DWH4-release-keys
- Repo: samsung_m22_dump
