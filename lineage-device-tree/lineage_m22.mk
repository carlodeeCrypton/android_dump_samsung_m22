#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from m22 device
$(call inherit-product, device/samsung/m22/device.mk)

PRODUCT_DEVICE := m22
PRODUCT_NAME := lineage_m22
PRODUCT_BRAND := samsung
PRODUCT_MODEL := SM-M225FV
PRODUCT_MANUFACTURER := samsung

PRODUCT_GMS_CLIENTID_BASE := android-samsung-ss

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="m22jvxx-user 13 TP1A.220624.014 M225FVXXU8DWH4 release-keys"

BUILD_FINGERPRINT := samsung/m22jvxx/m22:13/TP1A.220624.014/M225FVXXU8DWH4:user/release-keys
