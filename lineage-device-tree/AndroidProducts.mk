#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_m22.mk

COMMON_LUNCH_CHOICES := \
    lineage_m22-user \
    lineage_m22-userdebug \
    lineage_m22-eng
